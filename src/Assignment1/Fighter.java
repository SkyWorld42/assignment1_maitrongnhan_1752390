package Assignment1;

public class Fighter {
	private int mBaseHP;
	private int mWp; 
	
	public Fighter(int BaseHP, int Wp)
	{
		mBaseHP = BaseHP;
		mWp = Wp;
	}
	
	public int getBaseHP()
	{
		return mBaseHP;
	}
	
	public void setBaseHP(int baseHP)
	{
		mBaseHP = baseHP;
	}
	
	public int getWp()
	{
		return mWp;
	}
	
	public void setWp(int wp)
	{
		mWp = wp;
	}
	
	public double getScorebyWp()
	{
		if (this.getWp() == 1)
		{
			return this.getBaseHP();
		}
		else
		{
			return this.getBaseHP()/10;
		}
	}
	
	public double getCombatScore()
	{
		return 0;
	}
}
