package Assignment1;

public class Warrior extends Fighter {
	public Warrior(int baseHP, int wp)
	{
		super(baseHP, wp);
		if ( (baseHP < 1 || baseHP > 888) || (wp < 0 || wp > 3))
		{
			throw new IllegalArgumentException("Input parameter is out of range");
		}
	}
	
	public double getCombatScore()
	{
		if ( Utility.isPrime(Utility.Ground) )
		{
			return this.getBaseHP()*2;
		}
		else
		{
			return super.getScorebyWp();
		}
	}
}
