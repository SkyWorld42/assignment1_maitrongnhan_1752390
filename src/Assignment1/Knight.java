package Assignment1;

public class Knight extends Fighter {
	public Knight(int baseHP, int wp)
	{
		super(baseHP, wp);
		if ( (baseHP < 99 || baseHP > 999) || (wp < 0 || wp > 3))
		{
			throw new IllegalArgumentException("Input parameter is out of range");
		}
	}
	
	public double getCombatScore()
	{
		if (Utility.Ground == 999)
		{
			return Utility.getFib_largerthan(this.getBaseHP()*2);
		}
		if ( Utility.isSquare(Utility.Ground) )
		{
			return this.getBaseHP()*2;
		}
		else
		{
			return super.getScorebyWp();
		}	
	}
	
}
