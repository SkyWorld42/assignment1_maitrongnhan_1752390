package Assignment1;

public class Utility {
	public static int Ground;
	
	public static boolean isPrime(int num) 
	{
		if (num < 2) 
		{
			return false;
		}
		
		for (int i = 2; i < num / 2; i++) 
		{
			if (num % i == 0)
			{
				return false;
			}
		}
		
		return true;
	}

	public static boolean isSquare(int num)
	{
		int x = (int) Math.sqrt(num);
		return x*x == num;
	}
	
	public static int getFib_largerthan(int limit)
	{
		int result = 0;
		int prev = 0, current = 1;
		while (result < limit)
		{
			result = prev + current;
			prev = current;
			current = result;
		}
		return result;
	}
}
